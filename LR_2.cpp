#include <iostream>
using namespace std;

int main()
{
    float distance, fuel_per_100km, price;

    // printf scanf
    printf("Enter distance: ");
    scanf_s("%f", &distance);
    printf("Enter fuel per 100 km: ");
    scanf_s("%f", &fuel_per_100km);
    printf("Enter price of gas: ");
    scanf_s("%f", &price);
    float result = (price / fuel_per_100km) * distance * 2;
    printf("Price: %f\n", result);

        
    //// cout cin
    /*cout << "Enter distance: ";
    cin >> distance;
    cout << "Enter fuel per 100 km: ";
    cin >> fuel_per_100km;
    cout << "Enter price of gas: ";
    cin >> price;
    double result = (price / fuel_per_100km) * distance * 2;
    cout << "Price: " << result << endl;*/

    system("pause");

    return 0;
}
